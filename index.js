/**
 * Bài 1
 * Nhập ba số
 * Sắp xếp theo thứ tự nhỏ đến lớn
 * In ra kết quả */
document.getElementById("btn_sapxep").addEventListener("click", function () {
  var num1 = document.getElementById("txt-num1").value * 1;
  var num2 = document.getElementById("txt-num2").value * 1;
  var num3 = document.getElementById("txt-num3").value * 1;
  console.log({ num1, num2, num3 });
  if (num1 > num2 && num2 > num3) {
    document.getElementById("result_01").innerHTML =
      num3 + " < " + num2 + " < " + num1;
  } else if (num1 > num2 && num3 > num2 && num1 > num3) {
    document.getElementById("result_01").innerHTML =
      num2 + " < " + num3 + " < " + num1;
  } else if (num1 > num2 && num3 > num1 && num3 > num2) {
    document.getElementById("result_01").innerHTML =
      num2 + " < " + num1 + "< " + num3;
  } else if (num2 > num1 && num2 > num3) {
    document.getElementById("result_01").innerHTML =
      num3 + " < " + num1 + "< " + num2;
  } else if (num2 > num3 && num3 > num1) {
    document.getElementById("result_01").innerHTML =
      num1 + " < " + num3 + "< " + num2;
  } else {
    document.getElementById("result_01").innerHTML =
      num1 + " < " + num2 + "< " + num3;
  }
});
/**
 * Bài 2: Chào hỏi
 * Chọn thành viên trong gia đình
 * IN ra câu chào hỏi tương ứng
 */
document.getElementById("btn-Hello").addEventListener("click", function () {
  var pEL = document.getElementById("txt-Hello");
  var userValue = document.getElementById("selUser").value;
  pEL.innerHTML = "Xin chào Bố";
  switch (userValue) {
    case "B": {
      pEL.innerHTML = "Xin chào Bố";
      break;
    }
    case "M": {
      pEL.innerHTML = "Xin chào Mẹ";
      break;
    }
    case "A": {
      pEL.innerHTML = "Xin chào Anh trai";
      break;
    }
    case "E": {
      pEL.innerHTML = "Xin chào Em gái";
      break;
    }
    default: {
      pEL.innerHTML = "Xin chào người lạ";
    }
  }
});
/**
 * Bài 3: Đếm số chẳn lẽ
 * Nhập ba số nguyên
 * tạo biến đếm
 * nếu số nguyên chia hết cho 2 thì tăng biến đếm chẳn hoặc lẽ tương ứng
 * In ra kết quả
 */
document
  .getElementById("btn_tinhchanle")
  .addEventListener("click", function () {
    var num31Value = document.getElementById("txt-num31").value * 1;
    var num32Value = document.getElementById("txt-num32").value * 1;
    var num33Value = document.getElementById("txt-num33").value * 1;
    var demchanle = 0;
    console.log({ num31Value, num32Value, num33Value });
    if (num31Value % 2 == 0) {
      demchanle++;
    }
    if (num32Value % 2 == 0) {
      demchanle++;
    }
    if (num33Value % 2 == 0) {
      demchanle++;
    }
    console.log({ demchanle });
    document.getElementById("result3").innerHTML =
      "Có " + demchanle + " số chẵn, " + (3 - demchanle) + " số lẻ";
  });
/**
 * Bài 4: đoán hình dạng tam giác
 * Nhập độ dài ba cạnh
 * Nếu ba cạnh bằng nhau là tam giác đều
 * Nếu 2 cạnh bằng nhau là tam giác cân
 * nếu bình phương 1 cạnh bằng 2 cạnh còn lại thì là tam giác vuông
 */
document
  .getElementById("btn_doantamgiac")
  .addEventListener("click", function () {
    var num41Value = document.getElementById("txt-num41").value * 1;
    var num42Value = document.getElementById("txt-num42").value * 1;
    var num43Value = document.getElementById("txt-num43").value * 1;
    var edge1 = Math.sqrt(Math.pow(num41Value, 2) + Math.pow(num42Value, 2));
    var edge2 = Math.sqrt(Math.pow(num43Value, 2) + Math.pow(num42Value, 2));
    var edge3 = Math.sqrt(Math.pow(num43Value, 2) + Math.pow(num41Value, 2));
    if (num41Value == num42Value && num42Value == num43Value) {
      document.getElementById("result_04").innerHTML = "Hình tam giác đều";
    } else if (
      num41Value == num42Value ||
      num42Value == num43Value ||
      num41Value == num43Value
    ) {
      document.getElementById("result_04").innerHTML = "Hình tam giác cân";
    } else if (
      num41Value == edge2 ||
      num42Value == edge3 ||
      num43Value == edge1
    ) {
      document.getElementById("result_04").innerHTML = "Hình tam giác vuông";
    } else {
      document.getElementById("result_04").innerHTML =
        "Một loại tam giác nào đó";
    }
  });
